import 'package:flutter/material.dart';
import 'package:recipe_app/model/IngridentsModel.dart';
import 'package:recipe_app/model/RecipeModel.dart';
import 'package:recipe_app/screens/IngredientsCalculate.dart';
import 'package:uuid/uuid.dart';

class RecipeListing extends StatefulWidget {
  RecipeListing({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _RecipeListingState createState() => _RecipeListingState();
}

class _RecipeListingState extends State<RecipeListing> {
  final List<RecipeModel> _recipeList = <RecipeModel>[];
  final TextStyle _inputFont =
      const TextStyle(fontSize: 14, color: Colors.black);
  final TextStyle _descFont = const TextStyle(fontSize: 14, color: Colors.grey);
  final TextStyle _nameFont =
  const TextStyle(fontSize: 16, color: Colors.black);
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();

  var uuid = Uuid();

  @override
  Widget build(BuildContext context) {
    List<IngridentsModel> _ingridents = <IngridentsModel>[];
    for (int i = 1; i < 7; i++) {
      _ingridents.add(new IngridentsModel.initData(
          uuid.v1(), "ingrident " + i.toString(), i * 50.0, "g"));
    }
    _recipeList.clear();
    for (int i = 1; i < 6; i++) {
      _recipeList.add(new RecipeModel.initData(
          uuid.v1(),
          "Recipe " + i.toString(),
          'Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet' +
              i.toString(),
          _ingridents,
          i.toString() + " kg"));
    }
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: _buildIngridents(),
    );
  }

  @override // Add from this line ...
  Widget _buildIngridents() {
    return Container(
      margin: const EdgeInsets.all(8.0),
      child: ListView.builder(
        /* separatorBuilder: (context, index) => Divider(
                color: Colors.black,
              ),*/
          itemCount: _recipeList.length,
          itemBuilder: (BuildContext _context, int i) {
            return _buildRow(
                new IngridentsModel.initData(uuid.v1(), "entera name", 0, "--"),
                i);
          }),
    );
  }

  Widget _buildRow(IngridentsModel pair, int index) {
    return Card(
      child: ListTile(
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 80,
              width: 100,
              margin: const EdgeInsets.all(4.0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  //colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.6), BlendMode.dstATop),
                  image: new NetworkImage(
                      "https://giveitsomethyme.com/wp-content/uploads/2019/07/Ice-Cream-Sundae-Pie-Piled-High-Feature2-LR-RS.jpg"),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
              ),
            ),
            Expanded(child:
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.fromLTRB(8, 4, 4, 4),
                  child: Text(
                    _recipeList[index].name,
                    style: _nameFont,
                    maxLines: 1,
                    textAlign: TextAlign.start,
                    overflow: TextOverflow.fade,
                    softWrap: true,
                  ),
                ),

                Container(
                  margin: const EdgeInsets.fromLTRB(8, 4, 4, 4),
                  child:
                  Text(
                    _recipeList[index].description,
                    style: _descFont,
                    overflow: TextOverflow.fade,
                    maxLines: 3,
                    softWrap: true,
                  ),

                ),
              ],
            ),
            ),
            /*new IconButton(
            icon: new Icon(Icons.close),
            onPressed: () {
              setState(() {
                _recipeList
                    .removeWhere((item) => item.id == _recipeList[index].id);
                //_ingridents.removeAt(index);
              });
            },
          ),*/
          ],
        ),
        onTap: () {
          // Add 9 lines from here...
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  IngredientsCalculate(title: _recipeList[index].name),
              settings: RouteSettings(
                arguments: _recipeList[index],
              ),
            ),
          );
        }, // ... to here.

      ),
    );
  }

  Widget _buildItem(RecipeModel item, Animation animation) {
    return SizeTransition(
      sizeFactor: animation,
      child: Card(
        child: ListTile(
          title: Text(
            item.name,
            style: TextStyle(fontSize: 20),
          ),
        ),
      ),
    );
  }

  void _removeSingleItem() {
    int removeIndex = 2;
    // Remove item from data list but keep copy to give to the animation.
    RecipeModel removedItem = _recipeList.removeAt(removeIndex);
    // This builder is just for showing the row while it is still
    // animating away. The item is already gone from the data list.
    AnimatedListRemovedItemBuilder builder = (context, animation) {
      return _buildItem(removedItem, animation);
    };
    // Remove the item visually from the AnimatedList.
    _listKey.currentState.removeItem(removeIndex, builder);
  }
}
