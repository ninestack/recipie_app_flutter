import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:recipe_app/model/RecipeModel.dart';
import 'package:recipe_app/screens/AddRecipeFormB.dart';

class AddRecipeFormA extends StatefulWidget {
  AddRecipeFormA({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _AddRecipeFormAState createState() => _AddRecipeFormAState();
}

class _AddRecipeFormAState extends State<AddRecipeFormA> {
  final TextStyle _biggerFont = const TextStyle(fontSize: 18);
  String name = "",
      description = "";
  RecipeModel _recipeModel = new RecipeModel.initial();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
            title: Text(widget.title),
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, false),
            )),
        body: Container(
          margin: const EdgeInsets.all(8.0),
          child: new Stack(
            children: <Widget>[
              Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          margin: const EdgeInsets.all(8.0),
                          child: Text("Name", style: _biggerFont)),
                      Container(
                        margin: const EdgeInsets.all(8.0),
                        child: TextField(
                          decoration: InputDecoration(
                            contentPadding: new EdgeInsets.symmetric(
                                vertical: 6.0, horizontal: 6.0),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0)),
                          ),
                          style: new TextStyle(
                              fontSize: 16.0, color: Colors.black),
                          maxLines: 1,
                          keyboardType: TextInputType.multiline,
                          onChanged: (text) {
                            name = text;
                          },
                        ),
                      ),
                      Container(
                          margin: const EdgeInsets.all(8.0),
                          child: Text("Description",
                              textAlign: TextAlign.start, style: _biggerFont)),
                      Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(8.0),
                          child: TextField(
                            decoration: InputDecoration(
                              contentPadding: new EdgeInsets.symmetric(
                                  vertical: 6.0, horizontal: 6.0),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.0)),),
                            style:
                            new TextStyle(fontSize: 16.0, color: Colors.black),
                            maxLines: 5,
                            keyboardType: TextInputType.multiline,
                            onChanged: (text) {
                              description = text;
                            },
                          ),
                        ),
                      ),
                    ],
                  )),
              new Positioned(
                child: new Align(
                    alignment: FractionalOffset.bottomRight,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.all(8.0),
                          child: ButtonTheme(
                            buttonColor: Theme
                                .of(context)
                                .primaryColor,
                            textTheme: ButtonTextTheme.primary,
                            splashColor: Colors.blue[200],
                            minWidth: 200.0,
                            height: 60.0,
                            child: new RaisedButton(
                              onPressed: () {
                                if (name
                                    .trim()
                                    .isEmpty)
                                  _showToast("Enter name");
                                else if (description
                                    .trim()
                                    .isEmpty)
                                  _showToast("Enter description");
                                else {
                                  _recipeModel.name = name;
                                  _recipeModel.description = description;
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              AddRecipeFormB(
                                                  title: "Add Ingridents"),
                                          settings: RouteSettings(
                                              arguments: _recipeModel)));
                                }
                              },
                              child: new Text("NEXT", style: _biggerFont),
                            ),
                          ),
                        ),
                      ],
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _showToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.grey[850],
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
