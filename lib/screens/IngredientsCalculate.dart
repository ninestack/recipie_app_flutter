import 'package:flutter/material.dart';
import 'package:recipe_app/model/IngridentsModel.dart';
import 'package:recipe_app/model/RecipeModel.dart';
import 'package:uuid/uuid.dart';

class IngredientsCalculate extends StatefulWidget {
  IngredientsCalculate({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _IngredientsCalculateState createState() => _IngredientsCalculateState();
}

class _IngredientsCalculateState extends State<IngredientsCalculate> {
  var uuid = Uuid();
  RecipeModel _recipeModel = null;
  List<IngridentsModel> _ingrident = <IngridentsModel>[];
  final TextStyle _inputFont =
      const TextStyle(fontSize: 14, color: Colors.black);
  final TextStyle _textFont =
      const TextStyle(fontSize: 16, color: Colors.black);
  final TextStyle _buttonFont = const TextStyle(fontSize: 18);
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  int baseQuantity = 1;
  String baseUnit = " kg";

  @override
  Widget build(BuildContext context) {
    if (_ingrident.length == 0) {
      for (int i = 1; i < 7; i++) {
        _ingrident.add(new IngridentsModel.initData(
            uuid.v1(), "ingrident " + i.toString(), i * 50.0, "g"));
      }
      /* for (int i = 0; i < _ingrident.length; i++) {
        _ingrident[i].quantity = _ingrident[i].baseQuantity;
      }*/
      _recipeModel = new RecipeModel.initData(uuid.v1(), "chicken Biryani", "",
          _ingrident, baseQuantity.toString() + " " + baseUnit);
    }
    RecipeModel saved = ModalRoute
        .of(context)
        .settings
        .arguments;
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: true,
            // Here we take the value from the MyHomePage object that was created by
            // the App.build method, and use it to set our appbar title.
            title: Text(widget.title),
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, false),
            )),
        body: _buildIngridents(),
      ),
    );
  }

  @override // Add from this line ...
  Widget _buildIngridents() {
    return Container(
      height: double.maxFinite,
      child: new Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.all(16.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                          child: Text(
                        "Input Quantity",
                        style: _textFont,
                      )),
                    ),
                    Container(
                      width: 80,
                      child: TextField(
                        
                        textAlign: TextAlign.center,
                        keyboardType: TextInputType.number,
                        autofocus: false,

                        textAlignVertical: TextAlignVertical.center,
                        decoration: InputDecoration(

                            contentPadding: new EdgeInsets.symmetric(
                                vertical: 6.0, horizontal: 6.0),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0)),

                            hintText: baseQuantity.toString()),
                        style: _inputFont,

                        onChanged: (text) {
                          _calculateIngridents(text);
                        },
                      ),
                    ),
                    Container(
                        margin: const EdgeInsets.all(6.0),
                        child: Text(
                          "kg",
                          style: _textFont,
                        )),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.all(16.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                          child: Text(
                        "Base Quantity",
                        style: _textFont,
                      )),
                    ),
                    Container(
                      width: 100,
                      child: Text(
                        _recipeModel.defaultQuantity,
                        textAlign: TextAlign.center,
                        style: _inputFont,
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                margin: const EdgeInsets.fromLTRB(16, 16, 16, 4),
                height: 1,
                color: Colors.black54,
              ),
              Expanded(
                  child: ListView.builder(
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 156),
                      // The itemBuilder callback is called once per suggested
                      // word pairing, and places each suggestion into a ListTile
                      // row. For even rows, the function adds a ListTile row for
                      // the word pairing. For odd rows, the function adds a
                      // Divider widget to visually separate the entries. Note that
                      // the divider may be difficult to see on smaller devices.

                      itemCount: _recipeModel.ingridentsList.length,
                      itemBuilder: (BuildContext _context, int i) {
                        return _buildRow(
                            new IngridentsModel.initData(
                                uuid.v1(), "entera name", 0, "--"),
                            i);
                      })),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildRow(IngridentsModel pair, int index) {
    return ListTile(
      title: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(
            child: Text(
              _recipeModel.ingridentsList[index].name,
              style: _inputFont,
            ),
          ),
          Container(
            margin: const EdgeInsets.all(6.0),
            width: 80,
            child: Text(
              _recipeModel.ingridentsList[index].quantity.toString() +
                  " " +
                  (_recipeModel.ingridentsList[index].unit),
              style: _inputFont,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(IngridentsModel item, Animation animation) {
    return SizeTransition(
      sizeFactor: animation,
      child: Card(
        child: ListTile(
          title: Text(
            item.name,
            style: TextStyle(fontSize: 20),
          ),
        ),
      ),
    );
  }

  void _calculateIngridents(String text) {
    int quant = int.parse(text);
    if (quant == 0) quant = baseQuantity;
    setState(() {
      for (int i = 0; i < _ingrident.length; i++) {
        double quantity = _ingrident[i].baseQuantity;
        _ingrident[i].quantity = (quantity * quant);
        if (_ingrident[i].quantity >= 1000) {
          _ingrident[i].quantity = _ingrident[i].quantity / 1000;
          _ingrident[i].unit = "kg";
        } else
          _ingrident[i].unit = "g";
      }
    });
  }
}
