import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:recipe_app/model/IngridentsModel.dart';
import 'package:recipe_app/model/RecipeModel.dart';
import 'package:uuid/uuid.dart';

import 'Home.dart';

class AddRecipeFormB extends StatefulWidget {
  AddRecipeFormB({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _AddRecipeFormBState createState() => _AddRecipeFormBState();
}

class _AddRecipeFormBState extends State<AddRecipeFormB> {
  final List<IngridentsModel> _ingridents = <IngridentsModel>[];
  final TextStyle _inputFont =
  const TextStyle(fontSize: 14, color: Colors.black);
  final TextStyle _buttonFont = const TextStyle(fontSize: 18);
  final TextStyle _textFont =
  const TextStyle(fontSize: 16, color: Colors.black);
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  var uuid = Uuid();
  String baseQuantity = "";
  String quantityUnit = "--";
  RecipeModel recipeModel;

  @override
  Widget build(BuildContext context) {
    recipeModel = ModalRoute
        .of(context)
        .settings
        .arguments;

    if (_ingridents.length == 0) {
      setState(() {
        _ingridents.add(new IngridentsModel.initData(
            uuid.v1(), "enter name", 0, quantityUnit));
      });
    }
    return MaterialApp(
      title: 'Flutter Demo',
      routes: <String, WidgetBuilder>{
        '/home': (BuildContext context) => new Home(),
      },
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
            title: Text(widget.title),
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, false),
            )),
        body: new Stack(
          children: <Widget>[
            _buildIngridents(),
            new Positioned(
              child: new Align(
                  alignment: FractionalOffset.bottomRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.all(8.0),
                        child: ButtonTheme(
                          buttonColor: Theme
                              .of(context)
                              .primaryColor,
                          textTheme: ButtonTextTheme.primary,
                          splashColor: Colors.blue[200],
                          minWidth: 200.0,
                          height: 60.0,
                          child: new RaisedButton(
                            onPressed: () {
                              recipeModel.defaultQuantity = baseQuantity;
                              recipeModel.ingridentsList = _ingridents;
                              _addRecipe(recipeModel);
                              /*if (baseQuantity.trim().isEmpty) {
                                _showToast("enter quantity of recipe");
                              } else if (_ingridents.isEmpty) {
                                _showToast("enter ingredients for recipe");
                              } else {
                                if (_ingridents.isNotEmpty) {
                                  bool isError;
                                  for (int i = 0; i < _ingridents.length; i++) {
                                    if (_ingridents[i].name.isNotEmpty &&
                                        _ingridents[i].baseQuantity == 0) {
                                      _showToast("Please add quantity for " +
                                          _ingridents[i].name);
                                      isError = true;
                                      break;
                                    } else if ((_ingridents[i].name == null ||
                                            _ingridents[i].name.isEmpty) &&
                                        _ingridents[i].baseQuantity != 0) {
                                      _showToast(
                                          "Please add name for quantity added as " +
                                              _ingridents[i]
                                                  .baseQuantity
                                                  .toString() +
                                              " " +
                                              _ingridents[i].unit);
                                      isError = true;
                                      break;
                                    } else
                                      isError = false;
                                  }
                                  if (!isError) {
                                    recipeModel.defaultQuantity = baseQuantity;
                                    recipeModel.ingridentsList = _ingridents;
                                    //add_recipe api call
                                    _addRecipe(recipeModel);
                                  }
                                }
                              }*/
                            },
                            child: new Text("SUBMIT", style: _buttonFont),
                          ),
                        ),
                      ),
                    ],
                  )),
            )
          ],
        ),

      ),
    );
  }

  @override // Add from this line ...
  Widget _buildIngridents() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.all(16.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                    child: Text(
                      "Input Quantity",
                      style: _textFont,
                    )),
              ),
              Container(
                width: 80,
                child: TextField(
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.number,
                  autofocus: false,
                  decoration: InputDecoration(
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 6.0, horizontal: 6.0),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0)),
                  ),
                  style: _inputFont,
                  onChanged: (text) {
                    setState(() {
                      baseQuantity = text;
                    });
                  },
                ),
              ),
              Container(
                  margin: const EdgeInsets.all(8.0),
                  width: 20,
                  child: Text(quantityUnit)),
              Container(
                width: 45,
                child: new DropdownButton<String>(
                  items: <String>['kg', 'g', 'l', 'ml'].map((String value) {
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(value),
                    );
                  }).toList(),
                  onChanged: (String data) {
                    setState(() {
                      quantityUnit = data;
                    });
                  },
                ),
              ),
            ],
          ),
        ),
        Container(

          margin: const EdgeInsets.all(8.0),
          child: ButtonTheme(
            buttonColor: Theme
                .of(context)
                .primaryColor,
            textTheme: ButtonTextTheme.primary,
            splashColor: Colors.blue[200],

            child: new RaisedButton(
              onPressed: () {
                _addIngrident();
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.add),
                  new Text("Add Ingredient", style: _buttonFont),
                ],
              ),
            ),
          ),
        ),
        Flexible(
          fit: FlexFit.loose,
          child: Container(
            child: ListView.builder(
                // The itemBuilder callback is called once per suggested
                // word pairing, and places each suggestion into a ListTile
                // row. For even rows, the function adds a ListTile row for
                // the word pairing. For odd rows, the function adds a
                // Divider widget to visually separate the entries. Note that
                // the divider may be difficult to see on smaller devices.

                itemCount: _ingridents.length,
                itemBuilder: (BuildContext _context, int i) {
                  return _buildRow(
                      new IngridentsModel.initData(
                          uuid.v1(), "entera name", 0, "--"),
                      i);
                }),
          ),
        ),
      ],
    );
  }

  Widget _buildRow(IngridentsModel pair, int index) {
    return ListTile(
      title: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(
            child: TextField(
              autofocus: false,
              decoration: InputDecoration(
                contentPadding:
                new EdgeInsets.symmetric(vertical: 6.0, horizontal: 6.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0)),
              ),
              style: _inputFont,
              onChanged: (text) {
                setState(() {
                  _ingridents[index].name = text;
                });
              },
            ),
          ),
          Container(
            margin: const EdgeInsets.all(6.0),
            width: 40,
            child: TextField(
              autofocus: false,
              textAlign: TextAlign.center,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  contentPadding:
                  new EdgeInsets.symmetric(vertical: 6.0, horizontal: 6.0),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                  hintText: pair.baseQuantity.toString()),
              style: _inputFont,
              onChanged: (text) {
                setState(() {
                  _ingridents[index].baseQuantity = double.parse(text);
                });
              },
            ),
          ),
          Container(
              margin: const EdgeInsets.all(4.0),
              width: 20,
              child: Text(quantityUnit)),
          Container(
            width: 45,
            child: new DropdownButton<String>(
              items: <String>['kg', 'g', 'l', 'ml'].map((String value) {
                return new DropdownMenuItem<String>(
                  value: value,
                  child: new Text(value),
                );
              }).toList(),
              onChanged: (String data) {
                setState(() {
                  _ingridents[index].unit = data;
                });
              },
            ),
          ),
          new IconButton(
            icon: new Icon(Icons.close),
            onPressed: () {
              setState(() {
                _ingridents
                    .removeWhere((item) => item.id == _ingridents[index].id);
                //_ingridents.removeAt(index);
              });
            },
          )
        ],
      ),
    );
  }

  void _addIngrident() {
    setState(() {
      _ingridents.add(new IngridentsModel.initData(
          uuid.v1(), "enter name", 0, quantityUnit));
    });
  }

  Widget _buildItem(IngridentsModel item, Animation animation) {
    return SizeTransition(
      sizeFactor: animation,
      child: Card(
        child: ListTile(
          title: Text(
            item.name,
            style: TextStyle(fontSize: 20),
          ),
        ),
      ),
    );
  }

  void _removeSingleItem() {
    int removeIndex = 2;
    // Remove item from data list but keep copy to give to the animation.
    IngridentsModel removedItem = _ingridents.removeAt(removeIndex);
    // This builder is just for showing the row while it is still
    // animating away. The item is already gone from the data list.
    AnimatedListRemovedItemBuilder builder = (context, animation) {
      return _buildItem(removedItem, animation);
    };
    // Remove the item visually from the AnimatedList.
    _listKey.currentState.removeItem(removeIndex, builder);
  }

  void _showToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 2,
        backgroundColor: Colors.grey[850],
        textColor: Colors.white,
        fontSize: 16.0);
  }

  void _addRecipe(RecipeModel recipeModel) {
    //Navigator.pop(context, false);
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
    /*addRecipe(recipeModel).then((response) {
      if (response.statusCode > 200) {
        Navigator.of(context).pushNamedAndRemoveUntil(
            '/home', (Route<dynamic> route) => false);
      } else
        print(response.statusCode);
    }).catchError((error) {
      print('error : $error');
    });*/
  }
}
