import 'package:recipe_app/model/IngridentsModel.dart';

class RecipeModel {
  String id;
  String name;
  String description;
  List<IngridentsModel> ingridentsList;
  String defaultQuantity;

  //RecipeModel(this.id, this.name, this.ingridentsList, this.defaultQuantity);

  RecipeModel(
      {this.id, this.name, this.description, this.ingridentsList, this.defaultQuantity});

  factory RecipeModel.fromJson(Map<String, dynamic> json) =>
      new RecipeModel(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        ingridentsList: json["ingredients"],
        defaultQuantity: json["defaultQuantity"],
      );

  Map<String, dynamic> toJson() =>
      {
        "id": id,
        "name": name,
        "description": description,
        "ingredients": ingridentsList,
        "defaultQuantity": defaultQuantity,
      };

  RecipeModel.initData(String id, String name, String desc,
      List<IngridentsModel> ingridentsList, String baseQuantity)
      :
        this.id = id,
        this.name = name,
        this.description = desc,
        this.ingridentsList = ingridentsList,
        this.defaultQuantity = baseQuantity;

  RecipeModel.initial();
}
