class IngridentsModel {
  String id;
  String name;
  double baseQuantity;
  double quantity = 0;
  String unit;

  /*IngridentsModel(this.id, this.name, this.baseQuantity, this.unit) {
    quantity = baseQuantity;
  }*/

  IngridentsModel({
    this.id, this.name, this.baseQuantity, this.unit,
  });

  factory IngridentsModel.fromJson(Map<String, dynamic> json) =>
      new IngridentsModel(
        id: json["id"],
        name: json["name"],
        baseQuantity: json["baseQuantity"],
        unit: json["unit"],
      );

  Map<String, dynamic> toJson() =>
      {
        "id": id,
        "name": name,
        "baseQuantity": baseQuantity,
        "unit": unit,
      };


  IngridentsModel.initData(String id, String name, double baseQuantity,
      String unit)
      :
        this.quantity = baseQuantity,
        this.id = id,
        this.name = name,
        this.baseQuantity = baseQuantity,
        this.unit = unit;


}
