import 'dart:convert';

import 'package:recipe_app/model/RecipeModel.dart';

List<RecipeModel> allPostsFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<RecipeModel>.from(
      jsonData.map((x) => RecipeModel.fromJson(x)));
}

String postToJson(RecipeModel data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
