import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:recipe_app/model/RecipeModel.dart';

import '../Constants.dart';
import 'RemoteHelper.dart';

Future<List<RecipeModel>> getAllRecipe() async {
  final response = await http.get('$base_url/getAllRecipe');
  print(response.body);
  return allPostsFromJson(response.body);
}

Future<http.Response> addRecipe(RecipeModel recipeModel) async {
  final response = await http.post('$base_url/addRecipe',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.authorizationHeader: ''
      },
      body: postToJson(recipeModel));
  return response;
}
